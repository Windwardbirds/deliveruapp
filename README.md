# Laboratorio 2 - Paradigmas 2018

## Programación con Frameworks

En este laboratorio implementamos el frontend de una aplicacion web usando Reactjs.

## Decisiones de diseño

* Se implementó una componente funcional llamada NavBar que maneja y renderiza la barra de navegación en la parte superior. Esta misma no utiliza estados, aunque revisa las cookies al cargarse con el fin de saber si el usuario esta o no conectado en un momento dado. 

* Cuando un usuario inicia sesion se crea una cookie, guardandola en el localStorage bajo el nombre token. Este token es usados en varias componentes para obtener el id o tipo de usuario actual. Al cerrar sesion se borra el token en el localStorage.

* Los containers usan un prop loading con el fin de esperar a que componentDidMount obtenga y cambie el estado del componente. En dicho momento se renderiza otra vez con los datos nuevos.

* Registro y Profile usan tabs. Estos estan implementados en containers/tabs y usan props active para determinar cual tab esta activa.

* Profile obtiene inicialmente los datos del usuario actual. Si es provider renderiza un tab adicional para administrar el menu. Cada uno de los tabs llaman a su respectiva componente pasando user (id, isProvider) como prop.

* Se priorizó el funcionamiento de la App con la API, sin tener en cuenta estética como estílos. Una vez finalizado el funcionamiento general de esta se puede apuntar a este punto. Se utiliza EcmaScript versión 6, utilizando jsx, como es requerído en el proyecto.

* Para el manejo de Tabs se utilizó ayuda de las siguientes fuentes, moficando el estilo de las mismas. Estos archivos pueden ser localizados en el directorio /containers/tabs.
  1.  https://github.com/brigand/react-scratch/tree/master/src/tabs
  2.  https://www.youtube.com/watch?v=KlQagQQmwX0