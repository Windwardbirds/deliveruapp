require 'json'
require 'sinatra/base'
require 'sinatra/json'
require 'sinatra/namespace'
require_relative './models/model'
require_relative './models/user'
require_relative './models/consumer'
require_relative './models/provider'
require_relative './models/item'
require_relative './models/order'
require_relative './models/location'
require_relative './models/class_inherited_attributes'

# Main class of the application
class DeliveruApp < Sinatra::Application
  register Sinatra::Namespace

  enable :sessions unless test?

  configure :development do
    pid = begin
            File.read('./node.pid')
          rescue StandardError
            nil
          end

    if pid.nil?
      ## Start the node server to run React
      pid = Process.spawn('npm run dev')
      Process.detach(pid)
      File.write('./node.pid', pid.to_s)
    end
  end

  ## Function to clean up the json requests.
  before do
    begin
      if request.body.read(1)
        request.body.rewind
        @request_payload = JSON.parse(request.body.read, symbolize_names: true)
      end
    rescue JSON::ParserError
      request.body.rewind
      puts "The body #{request.body.read} was not JSON"
    end
  end

  register do
    def auth
      condition do
        halt 401 unless session.key?(:logged_id) || self.settings.test?
      end
    end
  end

  ## API functions
  namespace '/api' do
    get '/locations' do
      array = []
      Location.instances.values.each do |object|
        array << object.to_hash
      end
      json array
    end

    post '/login' do
      email = @request_payload[:email]
      password = @request_payload[:password]
      consumer_array = Consumer.filter('email' => email)
      provider_array = Provider.filter('email' => email)
      user_array = consumer_array + provider_array
      halt 401 unless user_array.any? # non existing user
      hash = user_array.first.to_hash # send user to a hash
      halt 403 unless hash['email'] == email && hash['password'] == password
      session[:logged_id] = hash['id']
      hash_response = { 'id' => hash['id'], 'isProvider' => hash['provider'] }
      hash_response.to_json
    end

    post '/logout' do
    end

    # Register a new consumer
    post '/consumers' do
      email = @request_payload[:email]
      password = @request_payload[:password]
      location = @request_payload[:location]
      halt 400 if email.nil? || password.nil? || location.nil?
      halt 409 if Consumer.exists?('email' => email) ||
                  Provider.exists?('email' => email)
      hash = { 'email' => email,
               'password' => password,
               'location' => location,
               'balance' => 0.0,
               'provider' => false }
      consumer = Consumer.from_hash(hash)
      consumer.save
      consumer.id.to_json
    end

    # Register a new provider
    post '/providers' do
      email = @request_payload[:email]
      password = @request_payload[:password]
      location = @request_payload[:location]
      store_name = @request_payload[:store_name]
      max_delivery_distance = @request_payload[:max_delivery_distance]
      halt 400 if email.nil? || password.nil? || location.nil?
      halt 409 if Provider.exists?('email' => email) ||
                  Consumer.exists?('email' => email)
      halt 400 if store_name.nil? || max_delivery_distance.nil?
      hash = { 'email' => email,
               'store_name' => store_name,
               'location' => location,
               'password' => password,
               'balance' => 0.0,
               'max_delivery_distance' => max_delivery_distance,
               'provider' => true }
      provider = Provider.from_hash(hash)
      provider.save
      provider.id.to_json
    end

    # add an item
    post '/items' do
      name = @request_payload[:name]
      price = @request_payload[:price]
      provider_id = @request_payload[:provider]
      halt 400 if name.nil? || price.nil? || provider_id.nil?
      halt 404 unless Provider.index?(provider_id)
      halt 409 if Item.exists?('provider' => provider_id, 'name' => name)
      hash = { 'name' => name,
               'price' => price,
               'provider' => provider_id }
      item = Item.from_hash(hash)
      item.save
      item.id.to_json
    end

    post '/items/delete/:id' do
      id = (@params[:id]).to_i
      halt 404 unless Item.index?(id) # non existing item
      id_obj = Item.find(id)
      if ENV["RACK_ENV"].eql?("test")
        Item.delete(id)
      else
        halt 403 unless id_obj.provider == session['logged_id']
        Item.delete(id)
      end
    end

    # get all registered providers from this location
    get '/providers' do
      location = (@params[:location]).to_i
      if location.nil? || location == 0 # to_i parse nil into 0.
        providers = Provider.all
      else
        halt 404 unless Location.exists?('id' => location) # no location
        providers = Provider.filter('location' => location)
      end
      json_response = []
      providers.each do |object|
        h = object.to_hash
        hash_response = { 'id' => h['id'],
                          'email' => h['email'],
                          'location' => h['location'],
                          'store_name' => h['store_name'] }
        json_response << hash_response
      end
      json json_response
    end

    # get all registered consumers
    get '/consumers', :auth => nil do
      consumers = Consumer.all # self.all returns a list
      json_response = []
      consumers.each do |object|
        h = object.to_hash
        hash_response = { 'id' => h['id'],
                          'email' => h['email'],
                          'location' => h['location'] }
        json_response << hash_response
      end
      json json_response
    end

    post '/users/delete/:id' do
      id = (@params[:id]).to_i
      if Consumer.index?(id)
        Consumer.delete(id)
      elsif Provider.index?(id)
        Provider.delete(id)
      else
        halt 404 # non existing user
      end
    end

    get '/items' do
      provider_id = (@params[:provider]).to_i
      if ENV["RACK_ENV"].eql?("test") && provider_id == 0
        items_object = Item.all
      else
        provider = Provider.find(provider_id)
        halt 404 if provider.nil?
        items_object = Item.filter('provider' => provider_id)
      end
        json_response = []
        items_object.each do |object|
          json_response << object.to_hash
      end
      json json_response
    end

    post '/orders' do
      cons_id = @request_payload[:consumer].to_i
      items = @request_payload[:items]
      prov_id = @request_payload[:provider].to_i
      halt 400 if cons_id == 0 || items.empty? || prov_id == 0
      halt 404 unless Consumer.index?(cons_id) && Provider.index?(prov_id)
      items.each { |hash| halt 404 unless Item.index?(hash[:id]) }
      consumer = Consumer.find(cons_id)
      provider = Provider.find(prov_id)
      price = Order.total_price(items)
      provider.balance += price
      consumer.balance -= price
      order_hash = { 'provider' => provider.id,
                     'provider_name' => provider.store_name,
                     'consumer' => consumer.id,
                     'consumer_email' => consumer.email,
                     'consumer_location' => consumer.location,
                     'order_amount' => price,
                     'status' => 'payed',
                     'items' => items,
                     'balance_p' => provider.balance,
                     'balance_c' => consumer.balance }
      order = Order.from_hash(order_hash)
      order.save
      order.id.to_json
    end

    get '/orders/detail/:id' do
      id = (@params[:id]).to_i
	    halt 400 if id.nil? || id == 0
      order_object = Order.filter('id' => id)
      halt 404 if order_object.empty?
      h_order = order_object.first
      json_response = []
      h_order.items.each do |item_hash|
        item = Item.find(item_hash[:id])
        hash = { 'id' => item.id,
                 'name' => item.name,
                 'price' => item.price,
                 'amount' => item_hash[:amount] }
        json_response << hash
      end
      json json_response
    end

  get '/orders' do
    user_id = (@params[:user_id]).to_i
    halt 400 if user_id.nil? || user_id == 0
    if Consumer.index?(user_id)
      order_array = Order.filter('consumer' => user_id)
    elsif Provider.index?(user_id)
      order_array = Order.filter('provider' => user_id)
    else
      halt 404
    end
    json_response = []
    order_array.each do |object|
      order_hash = object.to_hash
      json_response << order_hash
    end
    json json_response
  end

    post '/deliver/:id' do
      order_id = (@params[:id]).to_i
      halt 404 unless Order.index?(order_id) # non existing order
      order = Order.find(order_id)
      order.status = 'delivered'

    end

    post '/orders/delete/:id' do
      id = (@params[:id]).to_i
    	unless Order.exists?(id)
    		Order.delete(id)
      end
    end

    get '/users/:id' do
      user_id = (@params[:id]).to_i
      halt 400 if user_id.nil? # no user id
      consumer = Consumer.find(user_id)
      provider = Provider.find(user_id)
      if !consumer.nil?
        hash = consumer.to_hash
      elsif !provider.nil?
        hash = provider.to_hash
      else
        halt 404 # non existing user
      end
      hash_response = { 'email' => hash['email'], 'balance' => hash['balance'] }
      json hash_response
    end

    post '/finish/:id' do
      order_id = (@params[:id]).to_i
      order = Order.find(order_id)
      order.status = 'finished'
      Order.save # save on data base.
      order.id.to_json
    end

    get '*' do
      halt 404
    end
  end

  # This goes last as it is a catch all to redirect to the React Router
  get '/*' do
    erb :index
  end
end
