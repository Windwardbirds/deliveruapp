import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// Alphabetically ordered components
import GetProviders from './containers/getproviders';
import Login from './containers/login'
import Logout from './containers/logout';
import Main from './containers/main';
import NavBar from './navigator'
import NotFound from './presentational/notfound';
import SingUp from './containers/singup';
import OrderConsumer from './containers/orderconsumer';
import UserId from './containers/userid';
import ManageMenu from './containers/managemenu';

export default function App() {
  return (
    <div>
      <BrowserRouter>
        <div>
        <NavBar />
        <Switch>
          <Route exact path="/" component={Main} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/singup" component={SingUp} />
          <Route path="/profile/:id?" component={UserId} />
          <Route exact path="/menu/:id" component={OrderConsumer} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/delivery/:id" component={GetProviders} />
          <Route component={NotFound} />
        </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
}
