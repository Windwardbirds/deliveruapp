import React from 'react';
import axios from 'axios';
import Menu from '../presentational/menu';

export default class ManageMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      user: this.props.user,
      itemlist: [],
      to_add: [],
      to_del: []
    };
  }

  componentDidMount() {
    var trueid = this.state.user.id
      axios
        .get("/api/items?provider=" + trueid)
        .then(
          response => this.setState({itemlist: response.data, loading: false})
        ).catch(
          error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
          }
      )
  }

  handleadditem(e){
    this.setState({loading: true}, () => {
      axios
        .post("/api/items", e)
        .then(
          response => this.setState({itemid: response.data, loading: false})
        ).catch(
          error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
          }
      )
    })
  }

  handledeleteitem(e){
    this.setState({loading: true}, () => {
      axios
        .post("/api/items/delete/" + e)
        .then(
          response => this.setState({loading: false})
        ).catch(
          error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
          }
      )
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    var newlist = this.state.itemlist.slice();
    var additions = this.state.to_add.slice();
    if (newlist.find((e) => e.name === event.target.itemname.value)){
      return alert('Ya existe un elemento con este nombre')
    }
    const newitem = {
      name: event.target.itemname.value,
      price: event.target.price.value,
      provider: this.state.user.id
    };
    newlist.push(newitem);
    additions.push(newitem);
    this.setState({itemlist: newlist, to_add: additions});
  }

  handleClick(event){
    var newlist = this.state.itemlist.slice();
    var additions = this.state.to_add.slice();
    var deletions = this.state.to_del.slice();
    const index = event.index;
    newlist.splice(index, 1);
    if (event.id) {
      deletions.push(event.id);
    } else {
      additions = additions.filter((e) => e.name !== event.name)
    }
    this.setState({
      itemlist: newlist,
      to_del: deletions,
      to_add: additions
    });
  }

  handleSave(event){
    this.state.to_add.map((e) => this.handleadditem(e));
    this.state.to_del.map((e) => this.handledeleteitem(e))

    this.setState({to_add: [], to_del: []});
  }

  render() {
    return (
      <Menu loading={this.state.loading}
            itemlist={this.state.itemlist}
            onSubmit={ (e) => this.handleSubmit(e)}
            onClick={ (e) => this.handleClick(e) }
            onClickSave={ (e) => this.handleSave(e) }/>
    );
  }
}
