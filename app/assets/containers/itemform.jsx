import React from 'react';
import axios from 'axios';
import Menu from '../presentational/menu';

export default class ItemForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: this.props.loading,
      item: {},
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    this.setState({item:{
      name: event.target.itemname.value,
      price: event.target.price.value,
      provider: this.props.id
    }}, function(){
      //console.log(this.state);
      this.props.submitform(this.state.item);
    })
    event.preventDefault();
  }

  render() {
    return (
      <Menu loading={this.state.loading}
            itemlist={this.props.itemlist}
            onSubmit={ (e) => this.handleSubmit(e)} />
    );
  }
}
