import axios from 'axios';
import React from 'react';
import Tabs from './tabs/Tabs.jsx';
import Form from '../presentational/register_form';
import Main from './main';

export default class Register extends Main {

  constructor(props) {
    super(props);
    this.state = { active: 'aTab',
                   user: {},
                   locations: this.state.locations,
                   location: 1,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleConsumer(user) {
    this.setState({loading: true}, () => {
      axios
        .post(
          'api/consumers',
          user
        ).then(response => {
           this.setState({user:response.data,
                          loading: false})
           this.props.history.push("/login")
        }).catch(error => {
          if (error.response.status === 400)
            alert("400 no email/location/password");
          else if (error.response.status === 409)
            alert("error 409 existing user");
          else
            alert("otro error");
        });
    } );
  }

  handleProvider(user) {
    this.setState({loading: true}, () => {
      axios
        .post(
          'api/providers',
          user
        ).then(response => {
            this.setState({user:response.data,
                           loading: false})
            this.props.history.push("/login")
        }).catch(error => {
          if (error.response.status === 400)
            alert("400 no email/location/store_name/password/distance");
          else if (error.response.status === 409)
            alert("error 409 existing user ");
          else
            alert("otro error");
        });
    } );
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + event.target.email.value);
    if (this.state.active === 'aTab') {
    this.setState({user:{
      email: event.target.email.value,
      password: event.target.password.value,
      location: parseInt(this.state.location),
    }}, function(){
      this.handleConsumer(this.state.user);
    })
    } else {
      this.setState({user:{
        email: event.target.email.value,
        password: event.target.password.value,
        store_name: event.target.store_name.value,
        location: parseInt(this.state.location),
        max_delivery_distance: event.target.max_delivery_distance.value,
      }}, () => {
        this.handleProvider(this.state.user);
      })
    }
    event.preventDefault();
  }

  handleChange(event) {
    this.setState({
      location: event.target.value,
    });
  }

  render() {
    const content = {
      aTab: <Form active='aTab'
                  onSubmit={ e => this.handleSubmit(e) }
                  locations={this.state.locations}
                  handleChange={e => this.handleChange(e)} />,
      
      bTab: <Form active='bTab'
                  onSubmit={ e => this.handleSubmit(e) }
                  locations={this.state.locations} 
                  handleChange={this.handleChange} />
    };

    return (
          <div >
            <p></p>
            <Tabs
              active={this.state.active}
              onChange={ active => this.setState({active})}
            >
              <div key="aTab">Registro de Usuarios</div>
              <div key="bTab">Registro de Deliveries</div>
            </Tabs>
            {content[this.state.active]}
          </div>
    );
  }
}
