import React from 'react';
import PropTypes from 'prop-types';
import Tabs from './tabs/Tabs';
import GetOrders from './getorders';
import Form from '../presentational/register_form';
import Profile from './profile';
import ManageMenu from './managemenu';
import User from '../presentational/user';


export default class UserId extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      active: 'aTab',
      user: {}
    }
  }

  componentDidMount() {
    var cookie = JSON.parse(window.localStorage.token)
    if (cookie){
      //console.log(cookie)
      this.setState({user:{
        id: cookie.id,
        isProvider: cookie.isProvider
      }, loading: false}, function(){
      })
    }
  }

  render() {

    if (this.state.user.isProvider)
     return (
      <User loading={this.state.loading}
            active={this.state.active}
            onChange={ active => this.setState({active})}
            user={this.state.user} />
     );
    return (
      <User loading={this.state.loading}
            active={this.state.active}
            onChange={ active => this.setState({active})}
            user={this.state.user} />
    );
  }
}
