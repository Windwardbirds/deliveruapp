import React from 'react';
import axios from 'axios';
import SingIn from '../presentational/singin';

export default class NameForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    //alert('A name was submitted: ' + event.target.email.value);
    this.setState({user:{
      email: event.target.email.value,
      password: event.target.password.value
    }}, function(){
      this.props.submitform(this.state.user);
    })
    event.preventDefault();
  }

  render() {
    return (
      <SingIn onSubmit={ (e) => this.handleSubmit(e)} />
    );
  }
}
