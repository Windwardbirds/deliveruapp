import React from 'react';
import axios from 'axios';
import { push } from "react-router-dom";
import TakeOrder from '../presentational/takeorder';
import Providers from '../presentational/providers';

export default class OrderConsumer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      itemlist: [],
      total: 0,
      quantitylist: [],
      subtotals: [],
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id
      axios
        .get("/api/items?provider=" + id)
        .then(
          response => this.setState({itemlist: response.data, loading: false})
        ).catch(
          error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
          }
      )
  }

  handleOrders(event) {
    this.setState({loading: true}, () => {
      axios
        .post(
          '/api/orders',
          event
        ).then(response => {
          this.setState({loading: false})
        }).catch(error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
        })
    })
  }

  handleSubmit(event) {
    const consumerId = JSON.parse(window.localStorage.token).id;
    const providerId = parseInt(this.props.match.params.id);
    var newquantitylist = this.state.quantitylist.slice();
    newquantitylist = newquantitylist.filter((e) => (e.id) && (e.amount !== 0));
    const neworder = {
      provider: providerId,
      items: newquantitylist,
      consumer: consumerId,
    };
    if (newquantitylist.length != 0){
      this.handleOrders(neworder);
    } else {
      alert('Please select some items')
    }
    event.preventDefault();
  }


  handleChange(event) {
    const quantity = event.target.valueAsNumber
    const price = parseInt(event.target.placeholder)
    const id = parseInt(event.target.id)
    var newquantity = this.state.quantitylist.slice()
    const index = event.target.name
    var newsubtotals = this.state.subtotals.slice()

    newsubtotals[index] = price*quantity
    newquantity[index] = {id: id, amount: quantity}
    const newtotal = newsubtotals.reduce(function(acc, val) { return acc + val; });

    this.setState({
      total: newtotal,
      quantitylist: newquantity,
      subtotals: newsubtotals
    })
  }

  render() {
    return (
        <TakeOrder loading={this.state.loading}
                   itemlist={this.state.itemlist}
                   total={this.state.total}
                   subtotals={this.state.subtotals}
                   onSubmit={e => this.handleSubmit(e)}
                   onChange={e => this.handleChange(e)} />
    );
  }
}
