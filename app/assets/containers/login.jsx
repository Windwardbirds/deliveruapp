import React from 'react';
import axios from 'axios';
import SingIn from '../presentational/singin';
import { withRouter } from "react-router-dom";
import NameForm from './nameform'

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      currentlog: {}
    };
  }

  handleLogin(event) {
    this.setState({loading: true}, () => {
      axios
        .post(
          'api/login',
          event
        ).then(response => {
          window.localStorage.setItem('token', JSON.stringify(response.data))
          this.setState({currentlog:response.data,
                         loading: false})
          this.props.history.push("/profile")
        }).catch(error => {
          alert("401 non existing user, 403 incorrect password")
        });
    } );
  }

  render() {
    return (
      <NameForm submitform={ e => this.handleLogin(e)} />
    );
  }
}
