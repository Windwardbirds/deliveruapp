import React from 'react';
import axios from 'axios';
import Info from '../presentational/info';
import Login from './login';

export default class Profile extends Login {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      user: this.props.user,
      currentlog: {}
    };
  }

  componentDidMount() {
    var trueid = this.state.user.id

    this.setState({loading: true}, () => {
      axios
        .get("/api/users/" + trueid)
        .then(
          response => this.setState({currentlog: response.data, loading: false},
            )
        ).catch(
          error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
          }
        )
    })
  }

  render() {
    return (
      <Info loading={this.state.loading}
            currentlog={this.state.currentlog}/>
    );
  }
}
