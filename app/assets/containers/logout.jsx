import React from 'react';
import axios from 'axios';
import { withRouter } from "react-router-dom";

export default class Logout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({loading: true}, () => {
      axios
        .post(
          'api/logout'
        ).then(response => {
          //console.log(response.data)
          window.localStorage.removeItem('token')
          this.setState({currentlog:response.data,
                         loading: false})
          this.props.history.push("/")
        }).catch(error => {
          alert("logout")
        });
    } );
  }

  render() {
    return (<div></div>)
  }
}
