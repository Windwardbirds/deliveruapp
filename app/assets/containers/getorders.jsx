import React from 'react';
import axios from 'axios';
import Orders from '../presentational/orders';

export default class GetOrders extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      user: this.props.user,
      orderlist: []
    };
  }

  componentDidMount() {
    var trueid = this.state.user.id
    this.setState({loading: true}, () => {
      axios
        .get("/api/orders?user_id=" + trueid)
        .then(
          response => this.setState({orderlist: response.data, loading: false})
        ).catch(
          error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
          }
      )
    })
  }

  handlefinish(event){
    this.setState({loading: true}, () => {
      axios
        .post("/api/finish/" + event)
        .then(
          response => this.setState({loading: false},
            console.log(response.data))
        ).catch(
          error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
          }
      )
    })
  }

  handleClick(event){
    var newlist = this.state.orderlist.slice();
    const index = event.index;
    newlist[index].status = 'finished'
    console.log('your list so far', newlist);
    this.setState({loading: true, orderlist: newlist},
      this.handlefinish(event.id));
  }

  render() {
    return (
      <Orders loading={this.state.loading}
              orderlist={this.state.orderlist}
              isProvider={this.state.user.isProvider}
              onClick={(e) => this.handleClick(e)}/>
    );
  }
}
