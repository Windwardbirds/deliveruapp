import React from 'react';
import axios from 'axios';
//import Main from './main';
import Providers from '../presentational/providers';

export default class GetProviders extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      location: props.location,
      providers: [],
    };

  }

  componentDidMount() {
    const location = this.props.match.params.id
    this.setState({loading: true}, () => {
      axios
        .get("/api/providers?location="+location)
        .then(response => this.setState({
            loading: false, providers: response.data},
            ))
        .catch(
          error => {
            if (!error.response)
              alert(error);
            else if (error.response.data && error.response.status !== 404)
              alert(error.response.data);
            else
              alert(error.response.statusText);
            this.setState({loading: false});
          }
        )
    })
  }

  render() {
    return (
      <Providers providers={this.state.providers}
                 loading={this.state.loading} />
    );
  }
}
