import React from 'react';
import PropTypes from 'prop-types';

export default function SingIn(props) {
  let { onSubmit } = props;

  return (
    <form onSubmit={props.onSubmit}>
      <p></p>
      <h2>Iniciar Sesión</h2>
      <label className="h5 text-right">
        <p></p>
        Correo Electrónico:
        <input type="email" name="email" required />
        <p></p>
        Contraseña:
        <input type="password" name="password" required />
      </label>
      <p></p>
        <input type="submit" value="Submit" />
    </form>
  );
}

SingIn.propTypes = {
  onSubmit: PropTypes.func.isRequired
};
