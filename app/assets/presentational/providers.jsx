import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom"

export default function Providers(props) {
  let { loading, providers } = props;

  if (props.loading) {
    return (
      <div>Loading. Please wait.</div>
    );
  } else {
    if (providers.length === 0) {
      return (<div className="fa-3x my-3">
                No se encontraron deliveries en tu zona.
              </div>);
    }
    return (
      <div>
        <p></p>
        <h3>¡Elige tu Delivery!:</h3>
        <p></p>
        { providers.map((prov, i) => (
                  <div key={i}>
                    <p></p>
                    <Link to={"/menu/"+prov.id.toString()}>
                      { prov.store_name }
                    </Link>
                  </div>
              ))}
      </div>
    );
  }
}

Providers.propTypes = {
  loading: PropTypes.bool.isRequired,
  providers: PropTypes.array.isRequired
};
