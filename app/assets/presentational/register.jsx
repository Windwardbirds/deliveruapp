import React from 'react';
import PropTypes from 'prop-types';
import Form from './register_form';
import Tabs from '../containers/tabs/Tabs.jsx';

export default function Register(props) {
  let { active } = props;

  const content = {
      aTab: <Form active='aTab'
                  onSubmit={ e => this.handleSubmit(e) } />,
      bTab: <Form active='bTab'
                  onSubmit={ e => this.handleSubmit(e) } />,
    };

  return (
        <div >
          <p></p>
          <Tabs
            active={active}
            onChange={ active => props.setState({active})}
          >
            <div key="aTab">Resgistro de Usuarios</div>
            <div key="bTab">Resgistro de Deliveries</div>
          </Tabs>
          {content[active]}
        </div>
  );
}

Form.propTypes = {
  active: PropTypes.string.isRequired,
};