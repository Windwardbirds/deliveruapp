import React from 'react';
import PropTypes from 'prop-types';

export default function Menu(props) {
  let { loading, itemlist, onSubmit, onClick } = props;
  if (props.loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
        <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <div>
      {props.itemlist.map(function(d, idx){
        return (
          <li key={idx}>
            <button onClick={props.onClick.bind
                            (this, {id: d.id, index: idx, name: d.name})}
            >
              -
            </button>
            {d.name} ${d.price}
          </li>
        )
      })}
      <form onSubmit={props.onSubmit}>
        <p></p>
        <label className="h6 text-right">
          <input type="submit" value="+" />
          <input type="text" name="itemname" required/>
          <input type="number" name="price"
          min="0" max="999" step="1" required/>
        </label>
      </form>
      <p></p>
      <button onClick={props.onClickSave}>¡Cambia tu menu!</button>
    </div>
    );
  }
}

Menu.propTypes = {
  loading: PropTypes.bool.isRequired,
  itemlist: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired
};
