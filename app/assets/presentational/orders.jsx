import React from 'react';
import PropTypes from 'prop-types';

export default function Orders(props) {
  let { loading, orderlist, isProvider, onClick } = props;
  if (props.loading) {
    return (
      <div>Loading. Please wait.</div>
    );
  } else {
    return (
      <div>
        Listado De Ordenes
        {props.orderlist.map(function(d, idx){
          if (props.isProvider) {
            return (
              <li key={idx}>
                {d.consumer_email}: ${d.order_amount} {d.status}
                <p></p>
                {(d.status === 'payed') ? (
                  <button onClick={props.onClick.bind(this, {id: d.id, index: idx})}>
                    Pedido enviado
                  </button>
                ) : (
                  <div> ***** </div>
                )}
                <p></p>
              </li>
            )
          } else {
            return (
              <li key={idx}>
                {d.provider_name}: {d.order_amount} {d.status}
                <p></p>
                {(d.status === 'finished') ? (
                  <button>
                   Calificar
                  </button>
                ) : (
                  <div> ***** </div>
                )}
                <p></p>
              </li>
            )
          }
        })
      }
      </div>
    );
  }
}

Orders.propTypes = {
  loading: PropTypes.bool.isRequired,
  orderlist: PropTypes.array.isRequired,
  isProvider: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};
