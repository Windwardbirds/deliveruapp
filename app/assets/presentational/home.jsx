import React from 'react';
//import { Link } from "react-router-dom";
import PropTypes from 'prop-types';

export default function Home(props) {
  var { loading, locations, handleChange, handleSubmit } = props;

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
        <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <div className="body">
        <div className="main-div">
          <p></p>
          <h1 className="display-3 text-center mb-5">
            ¿Qu&eacute; vas a comer hoy?</h1>
          <p></p>
          <h5>Elige tu ubicación:</h5>
          <ul>
            <form onSubmit={handleSubmit} >
              <select onChange={handleChange}>
                { locations.map((loc, idx) => (
                  <option key={idx}
                          value={loc.id}
                          className={loc.id.toString()} >
                    { loc.name }
                  </option>
              ))}
              </select>
              <input type="submit" value="Buscar" />
            </form>
          </ul>
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  loading: PropTypes.bool.isRequired,
  locations: PropTypes.array.isRequired
};
