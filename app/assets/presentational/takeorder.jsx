import React from 'react';
import PropTypes from 'prop-types';

export default function Menu(props) {

  let { loading, itemlist, onSubmit, onChange, total, subtotals } = props;

  if (props.loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
        <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <label className="h5 text-right">
      <form onSubmit={onSubmit} >
      <p></p>
      {itemlist.map((d, idx) => (
          <li key={idx}
              className={d.name}>
            {d.name} ${d.price}
            <input className="right" type="number" defaultValue='0'
                   id={d.id} name={idx} placeholder={d.price}
                   onChange={onChange} min="0" max="999" step="1" />
            Subtotal:{props.subtotals[idx]}
          </li>
      ))}
      <p></p>
      Su cuenta: {total}
      <input type="submit" value="Ordenar" />
      </form>
    </label>
    );
  }
}

Menu.propTypes = {
  loading: PropTypes.bool.isRequired,
  itemlist: PropTypes.array.isRequired,
  total: PropTypes.number.isRequired,
  subtotals: PropTypes.array.isRequired,
};
