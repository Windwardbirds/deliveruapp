import React from 'react';
import PropTypes from 'prop-types';

export default function Form(props) {
  let { active, onSubmit, locations, handleChange } = props;

  if (active === 'aTab') {
    return (
      <form onSubmit={props.onSubmit}>
        <p></p>
        <label className="h5 text-right">
          <p></p>
          Correo Electrónico:
          <input type="email" name="email" required />
          <p></p>
          Contraseña:
          <input type="password" name="password" required />
          <p></p>
          Ubicación:
          <select onChange={handleChange}>
            { locations.map((loc, idx) => (
              <option key={idx}
                      value={loc.id}
              >
                { loc.name }
              </option>
              ))
            }
          </select>
        </label>
        <p></p>
          <input type="submit" value="Submit" />
      </form>
    );
  } else {
    return (
     <form onSubmit={props.onSubmit}>
        <p></p>
        <label className="h5 text-right">
          <p></p>
          Nombre del Delivery:
          <input type="text" name="store_name" required />
          <p></p>
          Correo Electrónico:
          <input type="email" name="email" required />
          <p></p>
          Contraseña:
          <input type="password" name="password" required />
          <p></p>
          Ubicación:
          <select onChange={handleChange}>
            { locations.map((loc, idx) => (
              <option key={idx}
                      value={loc.id}
              >
                { loc.name }
              </option>
              ))
            }
          </select>
          <p></p>
          Máximo nro de Cuadras:
          <input type="num" name="max_delivery_distance" required />
        </label>
        <p></p>
          <input type="submit" value="Submit" />
      </form>
    );
  }
}

Form.propTypes = {
  active: PropTypes.string.isRequired,
  locations: PropTypes.array.isRequired
};
