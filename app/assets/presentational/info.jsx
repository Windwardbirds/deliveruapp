import React from 'react';
import PropTypes from 'prop-types';

export default function Info(props) {
  let { loading, currentlog } = props;
  if (props.loading) {
    return(
      <div>Loading. Please wait.</div>
    );
  } else {
    return (
      <div>Email: {currentlog.email}
      <p></p>
      Balance: ${currentlog.balance}</div>
    );
  }
}

  Info.propTypes = {
    loading: PropTypes.bool.isRequired,
    currentlog: PropTypes.object.isRequired
  };
