import React from 'react';
import PropTypes from 'prop-types';
import Tabs from '../containers/tabs/Tabs.jsx'
import GetOrders from '../containers/getorders'
import Profile from '../containers/profile'
import ManageMenu from '../containers/managemenu'

export default function User(props) {
    let { active, loading, user, onChange } = props;

    if (loading) return (
      <div>Loading. Please wait.</div>
    )
    
    const contentprovider = {
      aTab: <Profile user={user} />,  
      bTab: <GetOrders user={user} />,
      cTab: <ManageMenu user={user} />
    };
    
    const contentconsumer = {
      aTab: <Profile user={user} />,
      bTab: <GetOrders user={user} />
    };

    if (user.isProvider) return (
        <div>
          <p></p>
          <Tabs
            active={active}
            onChange={ props.onChange.bind(active)}
          >
            <div key="aTab">Perfil</div>
            <div key="bTab">Órdenes</div>
            <div key="cTab">Menu</div>
          </Tabs>
          {contentprovider[active]}
        </div>
    );
    return (
      <div>
        <p></p>
        <Tabs
          active={active}
          onChange={ props.onChange.bind(active) }
        >
          <div key="aTab">Perfil</div>
          <div key="bTab">Órdenes</div>
        </Tabs>
        {contentconsumer[active]}
      </div>
    );
}

User.propTypes = {
  loading: PropTypes.bool.isRequired,
  active: PropTypes.string.isRequired,
};

