import React from 'react';
import { Link } from "react-router-dom"

export default function NavBar() {

  if (window.localStorage.token) return (
    <div>
       <Link to='/'>Home </Link>
       <Link to='/profile'>Perfil </Link>
       <Link to='/logout'>Cerrar Sesión </Link>
    </div>
  );
  return (
    <div>
       <Link to='/'>Home </Link>
       <Link to='/singup'>Registrarse </Link>
       <Link to='/login'>Iniciar Sesión </Link>
    </div>
  );
}
