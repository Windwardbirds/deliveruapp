require_relative './model'

# From this class will inherit consumer and provider
class User < Model
  # Instance attributes - sets getters and setters\
  attr_accessor :email, :password, :location, :balance, :provider
end
