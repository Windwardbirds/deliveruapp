require_relative './model'

class Location < Model

  @db_filename = 'locations.json'
  attr_accessor :name

  def self.validate_hash?(model_hash)
    model_hash.key?('name') && super
  end

end
