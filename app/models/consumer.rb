require_relative './model'
require_relative './model'

class Consumer < User
  @db_filename = 'consumers.json'

  def self.validate_hash?(model_hash)
    model_hash.key?('email') && model_hash.key?('password') &&
    model_hash.key?('location') && model_hash.key?('balance') && super
  end

end
