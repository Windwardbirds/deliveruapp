require_relative './model'

class Order < Model

  @db_filename = 'orders.json'

  attr_accessor :provider, :provider_name, :consumer, :consumer_email,
   :consumer_location, :order_amount, :status, :items
  # gets the total_price of all orders
  def self.total_price(items)
    price = 0
    items.each do |hash|
      item = Item.find(hash[:id])
      price += item.price.to_i * hash[:amount].to_i
    end
    price
  end

  def self.validate_hash?(model_hash)
    model_hash.key?('provider') && model_hash.key?('provider_name') &&
    model_hash.key?('consumer') && model_hash.key?('consumer_email') &&
    model_hash.key?('consumer_location') && model_hash.key?('order_amount') &&
    model_hash.key?('status') && model_hash.key?('status') &&
    model_hash.key?('items') && model_hash.key?('balance_p') &&
    model_hash.key?('balance_c') && super
  end

end
