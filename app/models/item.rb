require_relative './model'

class Item < Model

  @db_filename = 'items.json'

  attr_accessor :name, :price, :provider

  def self.validate_hash?(model_hash)
    model_hash.key?('name') && model_hash.key?('price') &&
    model_hash.key?('provider') && super
  end
end
