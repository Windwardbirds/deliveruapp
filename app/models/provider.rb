require_relative './model'
require_relative './user'

class Provider < User
  @instances = {}
  @db_filename = 'providers.json'

  # Instance attributes - sets getters and setters
  attr_accessor :store_name, :max_delivery_distance

  def self.validate_hash?(model_hash)
    model_hash.key?('email') && model_hash.key?('password') &&
    model_hash.key?('store_name') && model_hash.key?('balance') &&
    model_hash.key?('location') && model_hash.key?('provider') &&
    model_hash.key?('max_delivery_distance') && super
  end
end
